var app = angular.module("myApp", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "/"
    })
    .when("/posts", {
        templateUrl : "posts.html",
        controller : "postsCtrl"
    })
    .when("/posts/:post_num", {
        templateUrl : "post.html",
        controller : "postCtrl"
    })
    .when("/posts/:post_num/comments", {
        templateUrl : "comments.html",
        controller : "commentsCtrl"
    })
    .when("/comments", {
        templateUrl : "comments.html",
        controller : "commentsPostCtrl"
    });
})
app.controller("postsCtrl", function ($scope, $http, $routeParams) {
    if($routeParams.userId === undefined) {
        $http.get('http://jsonplaceholder.typicode.com/posts').then(
            function (response) {
                $scope.posts = response.data;
            }
        );
    }
    else {
        $http.get('http://jsonplaceholder.typicode.com/posts?userId=' + $routeParams.userId).then(
            function (response) {
                $scope.posts = response.data;
            }
        );
    }
});
app.controller("postCtrl", function ($scope, $http, $routeParams) {
    var num = $routeParams.post_num;
    $http.get('http://jsonplaceholder.typicode.com/posts/' + num).then(
        function(response) {
            $scope.post = response.data;
        }
    );
});
app.controller("commentsCtrl", function ($scope, $http, $routeParams) {
    var num = $routeParams.post_num;
    $http.get('http://jsonplaceholder.typicode.com/posts/' + num + '/comments').then(
        function(response) {
            $scope.comments = response.data;
        }
    );
});

app.controller("commentsPostCtrl", function ($scope, $http, $routeParams) {
    $http.get('http://jsonplaceholder.typicode.com/comments' + $routeParams.postId).then(
        function(response) {
            $scope.comments = response.data;
        }
    );
});
